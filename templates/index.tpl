<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>Fc2 RSS Searcher</title>
</head>
<body>
    <em>RSS検索</em>

    <form action="{$SCRIPT_NAME}" method="post">
    <dl>
    <dt>日付</dt>
    <dd><input type="text" name="input1" value="{$data.input1|escape:"html":"Shift_JIS"}" />
    <p>{$error.input1}</p></dd>

    <dt>URL</dt>
    <dd><input type="text" name="input2" value="{$data.input2|escape:"html":"Shift_JIS"}" />
    <p>{$error.input2}</p></dd>

    <dt>ユーザ名</dt>
    <dd><input type="text" name="input3" value="{$data.input3|escape:"html":"Shift_JIS"}" />
    <p>{$error.input3}</p></dd>

    <dt>サーバー番号</dt>
    <dd><input type="text" name="input4" value="{$data.input4|escape:"html":"Shift_JIS"}" />
    <p>{$error.input4}</p></dd>

    <dt>エントリーNo.</dt>
    <dd><input type="text" name="input5" value="{$data.input5|escape:"html":"Shift_JIS"}" />
    <p>{$error.input5}</p></dd>

    </dl>
    <input type="submit" value="検索" class="btn" />
    <input type="hidden" name="action" value="confirm" />
    </form>

    <tr><th>Date | </th><th>URL | </th><th>Title | </th><th>Description | </th></br>

    {section name=item loop=$search_result}
      {$search_result[item]["date"]}
      {$search_result[item]["url"]}
      {$search_result[item]["title"]}
      {$search_result[item]["description"]}
      </br>
    {/section}

</body>
</html>
